#include <vector>
#include <utility>
#include <cmath>


using namespace std;

bool integral_solns(long d, long z, int k) {
    double a = 1.0, b = -d;
    double c = (1.0 / 3.0) * (pow(d, 2) - ((k - pow(z, 3)) / d));
    double determinant = sqrt(pow(b, 2) - 4 * a * c);
    return (determinant == double(int(determinant))) && ((int(-b + determinant) % int(2 * a)) == 0);
}

pair <long, long> quadratic_roots(double a, double b, double c) {
    double determinant = sqrt(pow(b, 2) - 4 * a * c);
    return make_pair(long((-b + determinant) / (2 * a)), long((-b - determinant) / (2 * a))); 
}

pair <long, long> find_xy(long d, long z, int k) {
    double a = 1.0, b = -d;
    double c = 1.0 / 3 * (pow(d, 2) - ((k - pow(z, 3)) / d));
    return quadratic_roots(a, b, c);
}




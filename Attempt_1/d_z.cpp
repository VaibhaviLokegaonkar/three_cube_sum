#include <bits/stdc++.h>
#include "find_x_and_y.cpp"
#include <math.h>

using namespace std;

vector<long int> divisors(long long  number){
    vector<long int> v;
    for(long int i = 1; i <= sqrt(number); i++){
        if(number % i == 0){
            v.push_back(i);
        }
    }
    return v;
}


void find_xy_UTIL(vector<long int> all_divisors,int z,int k){
    for(auto d : all_divisors){
        if(integral_solns(d,z,k)){
            pair<long int,long int> xy = find_xy(d,z,k);
            cout<<"x : "<< xy.second;
            cout<<" y : "<< d - xy.second ;
            cout<<" z : "<< z;
            cout<<" x^3 +y^3 + z^3 : "<< pow(xy.second,3) + pow( d - xy.second ,3) + pow(z,3) << "\n";
        }
            
    }
}



void generate_dz(int bound,int k){
    for(int z = 0 ; z > -bound; z--){
        long long diffrence = k - z*z*z;
        find_xy_UTIL(divisors(diffrence),z,k);
    }
}

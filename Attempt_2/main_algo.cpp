#include <cmath>
#include "./Utilities/util.hpp"
#include <iostream>
using namespace std;

//Return the remainder

long case1(long a, long p) {
    return a % p;
}

long two_mod_three(long a, long p) {
    return long(pow(a, (2 * p - 1) / 3)) % p;
} 

long seven_mod_nine(long a, long p) {
    return long(pow(a, (p + 2) / 3)) % p;
}

bool cube_root_exists(long a, long p) {
    return long(pow(a, (p - 1) / 3)) % p;
}

long one_mod_three(long a, long p) {
    for (int i = 0; i < p; i ++) {
     if (int(pow(i, 3.0)) % p == a)  {
        return i;
    }
  }
  return 0;
}

// Step 3
long cubic_residue(long a, long p) {
    if (p == 3 || p == 2 || a % p == 0) {
        return case1(a, p);
    }
    if (p % 3 == 2) {
        return two_mod_three(a, p);
    }
    if (p % 9 == 7) {
        return seven_mod_nine(a, p);
    }
    if (p % 3 == 1 && cube_root_exists(a, p)) {
      return one_mod_three(a, p);
    }
    return -1;
}

int derivative(int a1){
    return 3*a1*a1 ;
}

int function(int a_t,int k){ //x^3 - k
    return a_t*a_t*a_t - k;
}

int next_power(int a_t,int t,int a1,int prime,int k){
    int power_p = (int)pow(prime,t+1);
    int next = (a_t - function(a_t,k)*inv(derivative(a1),prime)) % power_p;
    if (next < 0) return next += power_p;
    return next;
}

int powers_of_prime(int prime,int power,int k,int a1){
    //cout << "Helllo" << endl;
    if(derivative(a1) != 0){
        int a_t = a1;
        for(int p = 1; p < power;p++){
            a_t = next_power(a_t,p,a1,prime,k);
        }
        return a_t;
    }
    return -1;
}

// Step 4
pair <int, int> update(long T, long z) {
    return (T / 2 < z && z < T) ? make_pair(-T, z) : make_pair(T, z - T);
}

// Step 5
bool integral_solns(long s, long z, int k) {
    float sqrt_D = sqrt((-3 * pow(s, 2)) - (12 * (pow(z, 3) - k) / s));
    if (sqrt_D != float(int(sqrt_D))) {
        return false;
    }
    return true;
}

pair <int, int> x_and_y(long s, long z, int k) {
    float a = 1.0, b = -s, c = (pow(s, 3) - (k - pow(z, 3))) / (3.0 * s);
    return quadratic_roots(a, b, c);
}
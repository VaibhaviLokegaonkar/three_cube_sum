#ifndef GENERATE_H_INCLUDED
#define GENERATE_H_INCLUDED

#include <utility>
#include <vector>
#include <tuple>

using namespace std;

tuple <int, int, int, bool> special_case(int k);

bool eligible(int k);
vector <tuple <long, long, long>> solutions(int k);

int powers_of_prime(int prime,int power,int k,int a1);
long cubic_residue(long a, long p);
bool integral_solns(long s, long z, int k);

pair <int, int> update(long T, long z);

bool integral_solns(long s, long z, int k);
pair <int, int> x_and_y(long s, long z, int k);

#endif // GENERATE_H_INCLUDED
#include <vector>
#include <utility>
#include <cmath>
using namespace std;

vector <int> factors(int n) {
    int div = 1;
    vector <int> factors;
    while (div * div <= n) {
        if (n % div == 0) {
            factors.push_back(div);
            factors.push_back(n / div);
        }
        div ++;
    }
    return factors;
}

pair <long, long> quadratic_roots(float a, float b, float c) {
    float determinant = sqrt(pow(b, 2) - 4 * a * c);
    return make_pair(long((-b + determinant) / (2 * a)), long((-b - determinant) / (2 * a))); 
}

//Chinese Remainder Theorem

int inv(int a, int m) { 
	int m0 = m, t, q; 
	int x0 = 0, x1 = 1; 
	if (m == 1) 
	return 0; 
	while (a > 1) {  
		q = a / m; 
		t = m; 
		m = a % m, a = t; 
		t = x0; 
		x0 = x1 - q * x0; 
		x1 = t; 
	} 
	if (x1 < 0) 
	x1 += m0; 
	return x1; 
} 

int findMinX(int num[], int rem[], int k) {  
	int prod = 1; 
	for (int i = 0; i < k; i++) 
		prod *= num[i]; 
	int result = 0; 
	for (int i = 0; i < k; i++) { 
		int pp = prod / num[i]; 
		result += rem[i] * inv(pp, num[i]) * pp; 
	} 
	return result % prod; 
} 
//#ifndef GENERATE_H_INCLUDED
#define GENERATE_H_INCLUDED

#include <vector>
#include <utility>

using namespace std;

bool Euler_criterion(int a,int prime);
vector<int> seive(int bound);
vector<pair<int,int>> getFactorization(int x,vector<int> spf);
std::vector <int> factors(int n);
int inv(int a, int m);
pair <long, long> quadratic_roots(float a, float b, float c);
int findMinX(int num[], int rem[], int k);

//#endif // GENERATE_H_INCLUDED
